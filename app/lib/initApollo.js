import {ApolloClient, createNetworkInterface} from 'react-apollo';
import fetch from 'isomorphic-fetch';

let apolloClient = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
	global.fetch = fetch
}

// Replace this URL by your APIs simple endpoint URL:
const GRAPHCMS_API = 'https://api.graphcms.com/simple/v1/cj7gmkxeq0ela01148hh3agcj';

function createClient () {
	const networkInterface = createNetworkInterface({
		uri: GRAPHCMS_API,
		ops: {
			credentials: 'same-origin'
		}
	});
	return new ApolloClient({
		ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
		networkInterface: networkInterface
	})
}

export default function initClient () {
	// Make sure to create a new client for every server-side request so that data
	// isn't shared between connections (which would be bad)
	if (!process.browser) {
		return createClient()
	}

	// Reuse client on the client-side
	if (!apolloClient) {
		apolloClient = createClient()
	}
	return apolloClient
}
